import 'jspsych';
import { jsPsychPlugin } from '../../lib/jspsych_plugin';

class HelloWorldPlugin extends jsPsychPlugin {
    info = {
        name: 'hello-world',
        parameters: {
            full_name: {
                type: jsPsych.plugins.parameterType.STRING,
                default: undefined,
            },
        },
    }

    trial(display_element, trial) {
        display_element.innerHTML = `<p>Hello World!</p><p>Your name is ${trial.full_name}</p>`;
        this.finish_trial();
    }
}

jsPsych.plugins.hello_world = new HelloWorldPlugin();
